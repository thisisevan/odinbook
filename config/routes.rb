Rails.application.routes.draw do
  root 'static_pages#home'
  devise_for :users, without: :destroy, controllers: { omniauth_callbacks: 'omniauth_callbacks' }
  resources :users, only: [:index, :show]
  resources :friendships, only: [:create, :destroy] do 
  	member { get :accept }
  end
  resources :posts, only: [:create, :destroy]
  resources :likes, only: [:create, :destroy]
  resources :comments, only: :create
end
