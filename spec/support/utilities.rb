def full_title(page_title)
		base_title = "Odinbook"
		page_title.empty? ? base_title : "#{base_title} | #{page_title}"
end 

def sign_in(user)
	visit new_user_session_path
	fill_in "Email", with: user.email
	fill_in "Password", with: user.password
end