require 'rails_helper'
Warden.test_mode!

feature "UserPages", :type => :feature do
  
  let!(:user) { create(:user) }
  
  feature "#index" do

    feature "when not signed in" do
      it "redirects to sign in page" do
        visit users_path
        expect(page).to have_title(full_title('Sign in'))
      end
    end

    feature "when signed in" do
      before(:each) do 
        5.times { create(:user) }
        login_as(user, scope: :user)
        visit users_path
      end
      it "renders each user" do
        User.all.each do |u|
          expect(page).to have_content(u.name)
        end
      end
    end
  end

  feature "#show" do

    feature "when not signed in" do
      it "redirects to sign in page" do
        visit user_path(user)
        expect(page).to have_title(full_title('Sign in'))
      end
    end

    feature "when signed in" do
      before(:each) do 
        login_as(user)
        user.posts.build(content: "Test Post 1").save
        user.posts.build(content: "Test Post 2").save
        visit user_path(user)
      end
      
      it "is titled the user's name" do
        expect(page).to have_title(full_title(user.name))
      end

      it "renders each post" do
        user.posts.each do |post|
          expect(page).to have_content(post.content)
        end
      end
    end
  end
end
