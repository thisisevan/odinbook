require 'rails_helper'

Warden.test_mode!

feature "Liking And Unliking", :type => :feature do
	let!(:user) { create(:user) }

	before(:each) { login_as(user, scope: :user) }

	feature "Posts" do
		let!(:post_author) { create(:user) }
		let!(:post) { create(:post, user: post_author) }

		before(:each) do 
			visit user_path(post_author)
			click_link('Like')
		end

		it "adds the user to the post's liked_users" do
			expect(post.liked_users).to include(user)
		end

		it "adds the post to the user's liked posts" do
			expect(user.liked_posts).to include(post)
		end
	end
end
