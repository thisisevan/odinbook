require 'rails_helper'
Warden.test_mode!

feature "Commenting", :type => :feature do
	let!(:user) { create(:user) }
	let!(:post_user) { create(:user) }
	let!(:post) { create(:post, user_id: post_user.id) }
	let(:comment_content) { "A Comment" }
	before(:each) do
	  login_as(user, scope: :user)
	  visit user_path(post_user)
	  within first('.post') do
	  	fill_in('comment_content', with: comment_content)
	  	click_button("Add Comment")
	  end
	end

	it "shows the comment" do
		expect(page).to have_content(comment_content)
	end

	it "updates the user's comments to include the comment" do
		expect(user.comments.pluck(:content)).to include(comment_content)
	end

	it "updates the post's comments to inlude the comment" do
		expect(post.comments.pluck(:content)).to include(comment_content)
	end

end
