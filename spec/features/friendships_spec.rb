require 'rails_helper'

Warden.test_mode!

feature "Friendships", type: :feature do

  let!(:user) { create(:user) }
  let!(:friend) { create(:user) }

  before(:each) do
    login_as(user, scope: :user)
    visit users_path
  end

  feature "creation" do
    before(:each) { click_link "Add Friend" }

    it "adds the friend to the user's pending_friends" do
      expect(user.pending_friends).to include(friend)
    end

    it "adds the user to the friend's friends_to_accept list" do
      expect(friend.friends_to_accept).to include(user)
    end
  end

  feature "after creation" do
    before(:each) do
      click_link "Add Friend"
      logout
      login_as(friend, scope: :user)
      visit root_url
    end

    feature "accepting friend request" do
      before(:each) { click_link("Accept") }
      it "adds the user to the friend's accepted friends" do
        expect(friend.accepted_friends).to include(user)
      end

      it "adds the friend to the user's accepted friends" do
        expect(user.accepted_friends).to include(friend)
      end

      feature "unfriend and destroy friendship" do
        before do 
          visit users_path
          click_link "Unfriend"
        end

        it "removes the friend from the user's list of friends" do
          expect(user.friends).not_to include(friend)
        end

        it "removes the user from the friend's list of friends" do
          expect(friend.friends).not_to include(user)
        end
      end
    end

    feature "decline and #destroy" do
      before(:each) { click_link("Decline") }

      it "removes the friend the user's list of friends" do
        expect(user.friends).not_to include(friend)
      end

      it "removes the user from the friend's list of friends" do
        expect(friend.friends).not_to include(user)
      end
    end
  end
end