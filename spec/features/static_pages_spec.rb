require 'rails_helper'
Warden.test_mode!

feature "StaticPages", :type => :feature do
  
  feature "home" do

    let(:user) { create(:user) }
    let(:friend) { create(:user) }

	  it "has the title 'Home' " do
      visit root_path
	  	expect(page).to have_title(full_title('Home'))
	  end

  	feature "for non signed in users" do
      before(:each) { visit root_path }
  		scenario "when user clicks signin link" do
  			click_link("Sign in")
  			expect(page).to have_title(full_title("Sign in"))
			end
  		
  		scenario "when user clicks sign up link" do
  			click_link("Sign up")
  			expect(page).to have_title(full_title("Sign up"))
  		end
  		#TODO - test facebook link
  	end

  	feature "for signed in users" do
  		feature "the feed is rendered" do
        before(:each) do 
          user.friendships.build(friend_id: friend.id, status: :accepted).save
          friend.friendships.build(friend_id: user.id, status: :accepted).save
          friend.posts.build(content: "A friend's first post").save
          friend.posts.build(content: "A friend's second post").save
          login_as(user, scope: :user)
          visit root_path
        end
        scenario "the feed is rendered" do
          friend.posts.each_with_index do |post, index| 
            within("#post_#{index}") { expect(page).to have_content(post.content) }
          end
        end
      end
  	end
  end
end
