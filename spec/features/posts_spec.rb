require 'rails_helper'
Warden.test_mode!

feature "Posts", :type => :feature do
  let!(:user) { create(:user) }

  before(:each) do 
  	login_as(user, scope: :user)
  	visit root_url
  end

  describe "creation" do 
  	before(:each) do 
  		fill_in('post_content', with: 'test post')
  		click_button "Submit post"
  	end

  	it "creates the post" do
  		expect(user.posts.find_by(content: 'test post')).not_to be_nil
  	end

  	it "is visible in user#show" do
  		visit user_path(user)
  		expect(page).to have_content('test post')
  	end

    describe "and destruction" do
      it "removes the post from the user's posts" do
        expect { click_link "Remove post" }.to change(Post, :count).by(-1)
      end
    end
  end
end
