require 'rails_helper'

RSpec.describe Post, :type => :model do
  let(:post) { create(:post) }

  it "responds to user" do
  	expect(post).to respond_to(:user)
  end

  it "respnds to content" do
  	expect(post).to respond_to(:content)
  end

  describe "validations" do
  	it "has a valid factory" do
  		expect(post).to be_valid
  	end

  	it "is not valid with no user" do
  		post.user = nil
  		expect(post).not_to be_valid
  	end

  	it "is not valid with no content" do
  		post.content = " "
  		expect(post).not_to be_valid
  	end
  end

end
