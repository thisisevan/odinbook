require 'rails_helper'

RSpec.describe Friendship, :type => :model do

	let(:user) { create(:user) }
	let(:friend) { create(:user) }
	let(:user_side_friendship) { user.friendships.build(friend_id: friend.id,
																				              status: :sent) }
  describe "validations" do
  	it "is valid with a user_id, friend_id, and status" do
  		expect(user_side_friendship).to be_valid
  	end

  	it "is invalid without a user_id" do
  		user_side_friendship.user_id = nil
  		expect(user_side_friendship).not_to be_valid
  	end

  	it "is invalid without a user_id" do
  		user_side_friendship.friend_id = nil
  		expect(user_side_friendship).not_to be_valid
  	end

  	it "is invalid without a proper status" do
  		[:foo, nil].each do |status|
  			user_side_friendship.status = status
  			expect(user_side_friendship).not_to be_valid
  		end
  	end
	end
end
