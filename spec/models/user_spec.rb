require 'rails_helper'

RSpec.describe User, :type => :model do
  
  let(:user) { create(:user) }

  describe "validation" do

    #not testing standard devise creation

  	it "isn't valid without a blank name" do
  		user.name = " "
  		expect(user).not_to be_valid
  	end
    
  	it "isn't valid with a nil name" do
  		user.name = nil
  		expect(user).not_to be_valid
  	end

  	it "is valid with a name" do
  		user.name = "Bob"
  		expect(user).to be_valid
  	end

  	it "has an valid email" do
  		user.email = "a@b.com"
  		expect(user).to be_valid
  	end

  	it "has an invalid email" do
  		user.email = "a.com"
  		expect(user).not_to be_valid
  	end

    it "is invalid with a duplicate email" do
      user.email = "a@b.com"
      user.save
      u2 = create(:user)
      u2.email = "a@b.com"
      expect(u2).not_to be_valid
    end

  	describe "removes extra white space"
  		specify "for its email" do
  			user.email = "   e@x.com   "
  			user.save
  			expect(user.email).to eq("e@x.com")
  		end

  		specify "for its name" do
        user.name = "   Bob   Doe   "
        user.save
        expect(user.name).to eq("Bob Doe")
  		end
  end

  describe "friendship associations" do
    let(:friend) { create(:user) }
    let!(:friendship) { Friendship.create(user_id: user.id, 
                                          friend_id: friend.id, status: :sent) }
    it "creates a user side friendship" do
      expect(user.friendships.to_a).to include(friendship)
    end

    it "adds a friend to the user's friends" do
      expect(user.friends.to_a).to include(friend) 
    end

    it "destroys friendships when user is deleted" do
      friendships = user.friendships
      user.destroy
      friendships.each do |friendship|
        expect(Friendship.where(id: friendship.id)).to be_empty
      end
    end
  end
end