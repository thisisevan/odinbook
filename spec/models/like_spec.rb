require 'rails_helper'

RSpec.describe Like, :type => :model do

	let(:user) { create(:user) }
	let(:post) { create(:post) }
	let(:like) { Like.create(user: user, likeable: post) }

	describe "validations" do
		it "is valid" do 
			expect(like).to be_valid
		end

		it "is not valid without a user_id" do
			like.user_id = nil;
			expect(like).not_to be_valid
		end

		it "is not valid without a likeable_id" do
			like.likeable_id = nil;
			expect(like).not_to be_valid
		end

		it "is not valid with an unexpected likeable_type" do
			like.likeable_type = nil;
			expect(like).not_to be_valid
		end
	end
end
