require 'rails_helper'

RSpec.describe Comment, :type => :model do
  
  let!(:user) { create(:user) }
  let!(:post) { create(:post) }
  let!(:comment) { post.comments.build(user_id: user.id, 
  																			content: "A Comment") }
  describe "validations" do
  	it "is valid" do
  		expect(comment).to be_valid
  	end

  	it "is invalid without a user id" do
  		comment.user_id = nil
  		expect(comment).not_to be_valid
  	end

  	it "is invalid without a post id" do
  		comment.post_id = nil
  		expect(comment).not_to be_valid
  	end

  	it "is invalid without content" do
  		comment.content = "   "
  		expect(comment).not_to be_valid
  	end
  end
end
