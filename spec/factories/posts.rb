# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :post do
  	user
  	sequence(:content) { Faker::Lorem.sentence }
  end
end
