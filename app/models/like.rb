class Like < ActiveRecord::Base
	belongs_to :user
	belongs_to :likeable, polymorphic: true

	validates :user_id, presence: true
	validates :likeable_id, presence: true
	validates :likeable_type, inclusion: { in: ['Post', 'Comment'] }

	before_save :user_is_not_post_creator

	private

	def user_is_not_post_creator
		liked_item = likeable_type == 'Post' ? Post.find(likeable_id) : 
																					 Comment.find(likeable_id)
		liked_item.user != self.user
	end
end
