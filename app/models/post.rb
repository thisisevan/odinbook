class Post < ActiveRecord::Base

	belongs_to :user

	has_many :likes, as: :likeable, dependent: :destroy
	has_many :liked_users, through: :likes, source: :user

	has_many :comments, dependent: :destroy
	
	validates :user_id, presence: true
	validates :content, presence: true, length: { maximum: 10000 }

	default_scope -> { order('created_at DESC') }
end
