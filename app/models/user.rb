class User < ActiveRecord::Base
  
  has_many :friendships, dependent: :destroy
  has_many :friends, through: :friendships
  has_many :inverse_friendships, class_name: "Friendship", foreign_key: "friend_id"
  has_many :inverse_friends, through: :inverse_friendships, source: :user

  has_many :posts, dependent: :destroy

  has_many :likes, dependent: :destroy
  has_many :liked_posts, through: :likes, source: :likeable, source_type: "Post"
  
  has_many :comments, dependent: :destroy
  
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable,
         :omniauthable, omniauth_providers: [:facebook]

  validates :name, presence: true

  before_save :remove_whitespace_in_name
  
  after_create :send_welcome_email

  def self.from_omniauth(auth)
    where(provider: auth.provider, uid: auth.uid).first_or_create do |user|
  		user.email = auth.info.email
  		user.password = Devise.friendly_token[0,20]
  		user.name = auth.info.name
  	end
  end

  def friends?(user)
    self.friends.to_a.include?(user)
  end

  def pending_friends
    User.where(id: self.friendships.where(status: :sent).select(:friend_id))
  end

  def friends_to_accept
    User.where(id: self.friendships.where(status: :received).select(:friend_id))
  end

  def accepted_friends
    User.where(id: self.friendships.where(status: :accepted).select(:friend_id))
  end

  def likes?(likeable)
    self.likes.where(likeable: likeable).exists?
  end

  def feed
    Post.where(user_id: self.accepted_friends.pluck(:id) << self.id )
  end

  
  private
    def remove_whitespace_in_name
      self.name = self.name.strip.gsub(/\s+/, " ")
    end

    def send_welcome_email
      UserMailer.welcome(self).deliver
    end
end
