class StaticPagesController < ApplicationController
	def home
		if user_signed_in?
			@post_to_create = current_user.posts.build
			@posts = current_user.feed.paginate(page: params[:page])
		end
		store_location
	end
end
