class FriendshipsController < ApplicationController

  before_action :authenticate_user!

	def create
  	@to_friend_request = current_user.friendships.build(status: :sent, friend_id: params[:friend_id])
    @friend = User.find(params[:friend_id])
    @from_friend_request = @friend.friendships.build(status: :received, 
                                                     friend_id: current_user.id)
  	if @to_friend_request.save && @from_friend_request.save
      flash[:notice] = "Sent friend request"
      redirect_back_or_default
  	else
      flash[:error] = "Unable to send friend request"
      redirect_back_or_default
  	end
	end

  def destroy
    current_user.friendships.find_by(friend_id: params[:friend_id]).destroy
    @friend = User.find(params[:friend_id])
    @friendship = @friend.friendships.find_by(friend_id: current_user.id)
    @friendship.destroy
  	redirect_back_or_default
  end

  def accept
    @friendship = current_user.friendships.find_by(friend_id: params[:friend_id])
    @friendship.status = :accepted
    @friendship.save
   
    @friend = User.find(params[:friend_id])
    @friendship = @friend.friendships.find_by(friend_id: current_user.id)
    @friendship.status = :accepted
    @friendship.save

    flash[:notice] = "You are now friends with " + @friend.name
    redirect_back_or_default
  end

end
