class LikesController < ApplicationController

	before_action :authenticate_user!

	def create
		@like = current_user.likes.build(like_params)
		flash[:error] - "Unable to like post" unless @like.save
		redirect_back_or_default
	end

	def destroy
		@like = Like.find(params[:id])
		@like.destroy if @like.user == current_user
		redirect_back_or_default
	end

	private

	def like_params
		params.require(:likeable).permit(:likeable_id, :likeable_type)
	end
end
