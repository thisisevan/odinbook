class PostsController < ApplicationController

	before_action :authenticate_user!
	
	def create
		@post = current_user.posts.build(post_params)
		flash[:error] = "Couldn't create post.  It is either blank or too long." unless @post.save
		redirect_back_or_default
	end
	
	def destroy
		@post = current_user.posts.find_by(id: params[:id])
		if @post
			@post.destroy
			redirect_back_or_default
		else
			flash[:error] = "Post couldn't be destroyed"
			redirect_back_or_default
		end
	end

	private
		def post_params
			params.require(:post).permit(:content)
		end
end
