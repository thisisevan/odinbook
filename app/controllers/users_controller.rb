class UsersController < ApplicationController

	before_action :authenticate_user!
	
	def index
		@users = User.paginate(page: params[:page])
		store_location
	end

	def show
		@user = User.find(params[:id])
		@friends = @user.accepted_friends.paginate(page: params[:page])
		@posts = @user.posts.paginate(page: params[:page])
		store_location
	end
end
