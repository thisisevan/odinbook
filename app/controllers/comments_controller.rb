class CommentsController < ApplicationController

	def create
		@comment = current_user.comments.build(comment_params)
		flash[:error] = "Comment couldn't be made" unless @comment.save
		redirect_back_or_default
	end

	private

	def comment_params
		params.require(:comment).permit(:post_id, :content)
	end
end
