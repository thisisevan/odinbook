class UserMailer < ActionMailer::Base
  default from: "OdinBook@odinbook.herokuapp.com"

  def welcome(user)
  	@user = user
  	mail(to: user.email, subject: 'Welcome to OdinBook')
  end

end
