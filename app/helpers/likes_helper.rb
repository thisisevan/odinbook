module LikesHelper
	def like_id_from_likeable(likeable)
		Like.find_by(user: current_user, likeable: likeable).id
	end
end
