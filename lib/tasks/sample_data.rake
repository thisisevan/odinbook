namespace :db do 
	desc "Fill database with sample data"
	task populate: :environment do
		require 'populator'
		make_users
		friend_users
		make_posts
		make_likes
		make_comments
	end
	
	task populate_users: :environment do
		require 'populator'
		make_users
	end

	task populate_friends: :environment do
		require 'populator'
		friend_users
	end

	task populate_posts: :environment do
		require 'populator'
		make_posts
	end

	task populate_likes: :environment do
		require 'populator'
		make_likes
	end

	task populate_comments: :environment do
		require 'populator'
		make_comments
	end
end

def make_users
	User.populate 20 do |user|
		user.name = Faker::Name.name
		user.email = Faker::Internet.email
		user.encrypted_password = User.new(password: 'pasword').encrypted_password
		user.sign_in_count = 1
	end
end

def friend_users
	User.all.each do |user|
		3.times do
			friend = loop do
				f = User.all.sample
				return f unless user == f || user.friends.include?(f)
			end
			user.friendships.build(friend: friend, status: :accepted).save
			friend.friendships.build(friend: user, status: :accepted).save
		end
	end
end

def make_posts
	30.times do
		User.all.to_a.sample.posts.build(content: Faker::Lorem.paragraph(rand(4) + 1)).save
	end
end

def make_likes
	30.times do
		post = Post.all.to_a.sample
		rand_friend = post.user.friends.to_a.sample
		if Like.where(user: rand_friend, likeable: post).to_a.empty?
			Like.new(user: rand_friend, likeable: post).save
		end

	end
end

def make_comments
	40.times do
		post = Post.all.to_a.sample
		rand_friend = post.user.friends.to_a.sample
		if rand_friend
			rand_friend.comments.build(post: post, 
																 content: Faker::Lorem.paragraph(rand(2) + 1)).save
		end
	end
end
